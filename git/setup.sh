#!/bin/sh

# Quickly configure git.

USERNAME="Sean Deaton"
EMAIL="sean.deaton@me.com"
EDITOR="vim"
GPG_BOOL=true
GPG_PRIVATE_KEY_NAME="private.key"

git config --global user.email $EMAIL
git config --global user.name $USERNAME
git config --global core.editor $EDITOR


# GPG signing

# IDK
# https://stackoverflow.com/questions/52444915/git-gpg-signing-fails-without-a-clear-message
export GPG_TTY=$(tty)
gpg --import $GPG_PRIVATE_KEY_NAME
git config --global commit.gpgsign $GPG_BOOL
# Find this with:
# `gpg --list-secret-keys --keyid-format LONG <your_email>`
#git config --global user.signingKey <ADD>
