#!/bin/sh

#snap list

snap remove firefox
snap remove snap-store
snap remove gtk-common-themes
snap remove gnome-3-38-2004
snap remove core18
snap remove snapd-desktop-integration

rm -rf /var/cache/snapd/

apt autoremove --purge snapd

rm -rf ~/snap

cp ./firefox-no-snap /etc/apt/preferences.d/firefox-no-snap

sudo add-apt-repository ppa:mozillateam/ppa

apt update
apt install firefox
